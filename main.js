import Vue from 'vue'
import App from './App'
App.mpType = "app";

//1.引入univueapp组件
import univueapp from 'univueapp'
import config from './common/config/index.js'
Vue.use(univueapp, config); //这里第二个参数为配置项
Vue.prototype.$config = config

//2.引入store则可以使用vuex
import store from 'univueapp/store'
Vue.prototype.$store = store

//3.如需要混入store数据需引入该项
import {
	mixin
} from 'univueapp/mixin'
Vue.mixin(mixin());

Vue.config.productionTip = false; //关闭生产提示
const app = new Vue({
	store, //4.把 store对象提供给“store”选项，这可以把 store 的实例注入所有的子组件
	...App
});
app.$mount()