export default {
	title: "小Q传输", //系统标题名称
	message: function(option) { //自定义消息提示

	},
	init: null, //初始化方法,如: function(option){}
	homePage: "/pages/home/index", //系统首页地址(这里也可以是一个方法)
	loginPage: "/pages/login/index", //系统登录页地址(这里也可以是一个方法)
	platform: process.env.VUE_APP_PLATFORM, //平台类型(如: h5, mp-weixin, app-plus 等)
	autoLogin: null, //自动登录方法(静默处理)如:function(option,callback){} 第二个参数为继续请求的回调方法,登录完成后调用它才能继续上一次请求
	components: require.context('@/components/', true, /\w+\.(vue)$/), //自定义组件(不为空时则自动加载,为空时则不会自动加载)
	http: { //请求相关的配置
		genSign: null, //生成签名(可为空)(这是一个方法,function (options) {return ''; })
		getUrl: null, //获取此次请求地址(可为空)(这是一个方法,function (options) {return ''; })
		getBaseURL: null, //获取请求基础地址(可为空)(这是一个方法,function (options) {return ''; })
		interceptor: { //拦截器对象
			request: null, //请求拦截器(可为空)
			response: null, //响应拦截器(可为空)
		},
		options: { //请求默认全局配置(请求时传递会优先覆盖当前)
			//---------------附加请求参数-------------------
			showMsg: true, //显示错误提示(当失败提示不为空且当前为开启状态)
			loading: true, //开启加载中状态显示
			loadingTitle: '加载中...', //开启加载中状态提示文字内容
			baseURL: process.env.VUE_APP_URL ? process.env.VUE_APP_URL : '', //请求基础地址
			headToken: 'Authorization', //请求头部授权令牌KEY
			headSign: null, //请求头部签名KEY(可为空)
			//---------------------------------------------
			authTokenKey: null, //授权令牌保存的KEY(可为空)
			loginUserKey: null, //登录用户保存的KEY(可为空)
			//---------------附加响应参数-------------------
			respCodeField: 'code', //响应码值字段
			respDataField: 'data', //响应数据字段
			respMessageField: 'msg', //响应提示字段
			respOkCode: '0', //成功响应码值(成功响应,如果有多个则可以写成数组如: ['0','200'])
			respNotAuthCode: null, //未授权响应码(表示当前用户没有权限,针对没有权限返回对应的响应码)
			respNotLoginCode: '41009', //未登录响应码(表示登录已失效,如果有多个则可以写成数组如: ['41009','41008'])
			respNotLoginResponseStatus: null, //未登录状态码(表示未登录Response的响应码,如果有多个则可以写成数组如: ['401','402'])
			isOriginal: false, //返回原始数据(为true表示不做处理后台返回啥就返回啥,如果为false且响应数据字段不为空respDataField的成功请求会取数据字段返回)
			//---------------原生参数-----------------------
			timeout: 60000, //请求超时时间(单位:毫秒)
			dataType: 'json',
		}
	},
	api: { //接口相关的配置
		component: require.context('@/api/', true, /\w+\.(js)$/), //API接口组件(默认自动加载,不需要自动加载可将该项设置为null)	
	},
	auth: { //权限相关的配置
		loginUserKey: "loginUser", //授权用户存储KEY
		authTokenKey: "authToken", //授权令牌存储KEY 
		authSaveStorage: true, //授权持久化到Storage
		authExpiredTime: 0, //授权存储失效时间(单位: 毫秒),默认0表示不过期
	},
	cache: { //存储相关的配置
		storageKeys: [], //需要持久化存储到storage中的键KEY
	},
	storage: {
		db: 'uni', //存储类型:  uni (待支持类型: cookie, local, session)
		dbkey: 'uvstorage', //存储的KEY
	},
	site: {
		routeKey: 'siteRoute', //菜单存储KEY
		collapseKey: 'siteCollapse', //收缩存储KEY
		logoOpenKey: 'logoOpen', //是否显示LOGO存储KEY
	}
}